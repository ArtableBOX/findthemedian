use std::io;

fn main() {
    let mut num_list: Vec<f64> = Vec::new();

    println!("This program will find the median of any given list of numbers. Enter a number and it will add it to the list. The list will be printed out to you each time you enter a number. It can be a decimal point number or a whole number, negative or positive. Once you are done with your list, enter 'd' (lowercase) and the median will be calculated. However, if you want to quit the program, enter 'q' (lowercase) and it will break out of the loop. This program was made by Isaak Murad (with a lot of help from StackOverflow!).");

    loop {
        println!("Input: ");

        let mut inp: String = String::new();

        io::stdin().read_line(&mut inp).expect("Failure");
        
        let upd_inp: f64 = match inp.trim().parse() {
            Ok(num) => num,
            Err(_) => if inp.trim() == String::from("q") {
                break;
            } else if inp.trim() == String::from("d"){
                break
                {
                    println!("Calculating Median...");
                    calc_med(&num_list);
                }
            } else {
                continue;
            }
        };
        
        num_list.push(upd_inp);
        num_list.sort_by(|a, b| a.partial_cmp(b).unwrap());

        println!("Current List: {:?}", num_list);
    }
}

fn calc_med(num_list: &[f64]) {
    let nums = num_list.len();

    let median: f64 = if nums % 2 == 0 {
        let n = nums / 2;

        (num_list[n] + num_list[n - 1]) / 2.0

    } else {
        let n = (nums - 1) / 2;

        num_list[n]
    };
    println!("Median: {}", median)
}
