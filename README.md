# FindTheMedian
FindTheMedian was created by Isaak Murad.

## Installation

Clone the repository and delve into the 'target' folder. Then, delve into the 'release' folder and run the executable 'FindTheMedian'. This method only works on Linux. A method that would work for other operating systems would be to compile the 'main.rs' file yourself. It is located in src/main.rs

## Usage

```rust
/* This program will find the median of any given list of numbers. 
Enter a number and it will add it to the list. 
The list will be printed out to you each time you enter a number. 
It can be a decimal point number or a whole number, negative or positive. 
Once you are done with your list, enter 'd' (lowercase) and the median will be calculated. 
However, if you want to quit the program, enter 'q' (lowercase) and it will break out of the loop. 
This program was made by Isaak Murad (with a lot of help from StackOverflow!). */

fn main() {
    println!("Enjoy!");
    println!(" - Isaak ");
}
```

## Thanks

Thank you, StackOverflow.

## License

[MIT] LICENSE.txt
